/*
var server = require("./server");

server.iniciar();
*/
/*
var server = require("./server");
var router = require("./router");

server.iniciar(router.route);
*/
var server = require("./server");
var router = require("./router");
var requestHandlers = require("./requestHandlers");

var handle = {}
handle["/"] = requestHandlers.iniciar;
handle["/iniciar"] = requestHandlers.iniciar;
handle["/subir"] = requestHandlers.subir;

server.iniciar(router.route, handle);